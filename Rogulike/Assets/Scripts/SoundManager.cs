using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour{

    public static SoundManager instance;

    public AudioSource se;

    public float low = 0.95f;
    public float high = 1.05f;

    private void Awake() {

        if (instance == null) {

            instance = this;

        } else if (instance != this) {

            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    public void  PlaySingle(AudioClip clip) {

        se.clip = clip;
        se.Play();
    }

    public void RandomSE(params AudioClip[] clips) {

        int rndIndex = Random.Range(0, clips.Length);
        float rndPitch = Random.Range(low, high);

        se.pitch = rndPitch;
        se.clip = clips[rndIndex];
        se.Play();
    }
    
}
