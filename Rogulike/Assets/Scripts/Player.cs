using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour{

    private Rigidbody2D rd2d;

    public LayerMask blockingLayer;
    private BoxCollider2D boxCollider;

    //player status
    public float moveTime = 0.1f;
    public bool isMoveing = false;

    public int attackDamage = 1;
    private Animator animator;

    private int food;
    public Text foodText;
    private int foodPoint = 10;
    private int sodaPoint = 20;

    public AudioClip moveSE01;
    public AudioClip moveSE02;
    public AudioClip eatSE01;
    public AudioClip eatSE02;
    public AudioClip drinkSE01;
    public AudioClip drinkSE02;
    public AudioClip gameOverSE;

    void Start(){

        rd2d = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        food = GameManager.instance.food;
        foodText.text = "Food:" + food;
    }

    void Update(){

        if (!GameManager.instance.playerTurn){ return; }

        int horizontal = (int)Input.GetAxisRaw("Horizontal");
        int vertical = (int)Input.GetAxisRaw("Vertical");

        if(horizontal != 0){

            vertical = 0;

            if (horizontal == 1) {

                transform.localScale = new Vector3(1, 1, 1);

            } else if(horizontal == -1){

                transform.localScale = new Vector3(-1, 1, 1);
            }
        }else if (vertical != 0){

            horizontal = 0;
        }

        if(horizontal != 0 || vertical != 0){

            ATMove(horizontal,vertical);
        }
    }

    public void ATMove(int horizontal, int vertical) {

        food--;
        foodText.text = "Food:" + food;

        RaycastHit2D hit;

        bool canMove = Move(horizontal, vertical, out hit);

        if(hit.transform == null){

            GameManager.instance.playerTurn = false;

            return;
        }

        Damage hitComponent = hit.transform.GetComponent<Damage>();

        if(!canMove && hitComponent != null){

            OnCantMove(hitComponent);
        }

        CheckFood();
        GameManager.instance.playerTurn = false;
    }

    public bool Move(int horizontal, int vertical, out RaycastHit2D hit){

        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(horizontal, vertical);

        boxCollider.enabled = false;

        hit = Physics2D.Linecast(start, end, blockingLayer);

        boxCollider.enabled = true;

        if (!isMoveing && hit.transform == null){

            StartCoroutine(Movement(end));
            SoundManager.instance.RandomSE(moveSE01, moveSE02);

            return true;
        }

        return false;
    }

    IEnumerator Movement(Vector3 end) {

        isMoveing = true;

        float remainingDistance = (transform.position - end).sqrMagnitude;

        while(float.Epsilon < remainingDistance) {

            transform.position = Vector3.MoveTowards(transform.position, end, 1f / moveTime * Time.deltaTime);

            remainingDistance = (transform.position - end).sqrMagnitude;

            yield return null;
        }

        transform.position = end;

        isMoveing = false;
        CheckFood();
    }

    public void OnCantMove(Damage hit){

        hit.AttackDamage(attackDamage);

        animator.SetTrigger("Attack");
    }


    private void OnTriggerEnter2D(Collider2D collision){

        if (collision.tag == "Food"){

            SoundManager.instance.RandomSE(eatSE01, eatSE02);
            food += foodPoint;
            foodText.text = "Food:" + food;

            collision.gameObject.SetActive(false);

        }else if(collision.tag == "Soda"){

            SoundManager.instance.RandomSE(drinkSE01, drinkSE02);
            food += sodaPoint;
            foodText.text = "Food:" + food;

            collision.gameObject.SetActive(false);

        } else if(collision.tag == "Exit"){

            Invoke("Restart",1f);

            enabled = false;
        }
    }

    public void Restart(){

        SceneManager.LoadScene(0);
    }

    public void OnDisable(){

        GameManager.instance.food = food;
    }

    private void CheckFood(){

        if(food <= 0){

            SoundManager.instance.PlaySingle(gameOverSE);
            GameManager.instance.GameOver();
        }
    }

    public void EnemyAttack(int loss){

        animator.SetTrigger("Hit");

        food -= loss;
        foodText.text = "-" + loss + "Food:" + food;

        CheckFood();
    }
}
