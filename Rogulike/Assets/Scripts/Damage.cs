using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour{

    public int wallHp = 3;
    public Sprite dmgWall;

    public int enemyHp = 5;
    private Enemy enemy;

    private SpriteRenderer spriteRenderer;

    public AudioClip audioClip01;
    public AudioClip audioClip02;

    void Start(){

        spriteRenderer = GetComponent<SpriteRenderer>();
        enemy = GetComponent<Enemy>();
    }

    public void AttackDamage(int loss){

        if (gameObject.CompareTag("Wall")){

            SoundManager.instance.RandomSE(audioClip01, audioClip02);
            spriteRenderer.sprite = dmgWall;
            wallHp -= loss;

            if (wallHp <= 0) {

                gameObject.SetActive(false);
            }

        }else if (gameObject.CompareTag("Enemy")){

            SoundManager.instance.RandomSE(audioClip01, audioClip02);
            enemyHp -= loss;

            if (enemyHp <= 0) {

                enemy.Death();
            }
        }
    }
}
