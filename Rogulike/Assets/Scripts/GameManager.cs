using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{

    public static GameManager instance;
    BoardManager boardManager;

    public bool playerTurn = true;
    public bool enemyTurn = false;

    public int level = 1;

    private bool doingSetUp;
    public Text levelText;
    public GameObject levelImage;

    public int food = 100;

    private List<Enemy> enemies;

    private void Awake(){
        
        if(instance == null) {

            instance = this;

        }else if(instance != this){

            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        enemies = new List<Enemy>();

        boardManager = GetComponent<BoardManager>();

        InitGame();
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static public void Call(){

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    static private void OnSceneLoaded(Scene next, LoadSceneMode a){

        instance.level++;
        instance.InitGame();
    }

    public void InitGame(){

        doingSetUp = true;

        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();

        levelText.text = "Day:" + level;
        levelImage.SetActive(true);

        Invoke("HideLevelImage", 2f);

        enemies.Clear();

        boardManager.SetUpScene(level);
    }

    public void HideLevelImage(){

        levelImage.SetActive(false);
        doingSetUp = false;
    }

    void Update(){
        
        if(playerTurn || enemyTurn || doingSetUp){ return; }

        StartCoroutine(MoveEnemies());
    }

    public void AddEnemy(Enemy script){

        enemies.Add(script);
    }

    public void DestroyEnemy(Enemy script) {

        enemies.Remove(script);
    }

    IEnumerator MoveEnemies(){

        enemyTurn = true;

        yield return new WaitForSeconds(0.1f);

        if(enemies.Count == 0){

            yield return new WaitForSeconds(0.1f);
        }

        for(int i = 0; i < enemies.Count; i++){

            enemies[i].MoveEnemy();

            yield return new WaitForSeconds(0.1f);
        }

        playerTurn = true;
        enemyTurn = false; 
    }

    public void GameOver(){

        levelText.text = "GameOver";
        levelImage.SetActive(true);

        enabled = false;
    }
}
