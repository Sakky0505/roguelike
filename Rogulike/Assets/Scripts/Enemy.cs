using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour{

    public LayerMask blockingLayer;
    private BoxCollider2D boxCollider;

    //enemy status
    public float moveTime = 0.1f;
    public bool isMoveing = false;

    public int attackDamage = 5;
    private Animator animator;

    private Transform target;
    private bool skipMove = false;

    public AudioClip enemyAttack01;
    public AudioClip enemyAttack02;
    void Start() {

        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();

        target = GameObject.FindGameObjectWithTag("Player").transform;

        GameManager.instance.AddEnemy(this);
    }

    public void MoveEnemy(){

        if (!skipMove){

            skipMove = true;
            int xdir = 0;
            int ydir = 0;

            if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon){

                ydir = target.position.y > transform.position.y ? 1 : -1;

            }else{

                xdir = target.position.x > transform.position.x ? 1 : -1;
            }

            ATMove(xdir, ydir);

        }else{

            skipMove = false;
            return;
        }
    }

    public void ATMove(int horizontal, int vertical) {        

        RaycastHit2D hit;

        bool canMove = Move(horizontal, vertical, out hit);

        if (hit.transform == null) { return; }

        Player hitComponent = hit.transform.GetComponent<Player>();

        if (!canMove && hitComponent != null) {

            OnCantMove(hitComponent);
        }
    }

    public bool Move(int horizontal, int vertical, out RaycastHit2D hit) {

        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(horizontal, vertical);

        boxCollider.enabled = false;

        hit = Physics2D.Linecast(start, end, blockingLayer);

        boxCollider.enabled = true;

        if (!isMoveing && hit.transform == null){

            StartCoroutine(Movement(end));

            return true;
        }

        return false;
    }

    IEnumerator Movement(Vector3 end){

        isMoveing = true;

        float remainingDistance = (transform.position - end).sqrMagnitude;

        while (float.Epsilon < remainingDistance){

            transform.position = Vector3.MoveTowards(transform.position, end, 1f / moveTime * Time.deltaTime);

            remainingDistance = (transform.position - end).sqrMagnitude;

            yield return null;
        }

        transform.position = end;
        isMoveing = false;
    }

    public void OnCantMove(Player hit){

        hit.EnemyAttack(attackDamage);
        animator.SetTrigger("Attack");
        SoundManager.instance.RandomSE(enemyAttack01, enemyAttack02);
    }

    public void Death(){

        GameManager.instance.DestroyEnemy(this);
        gameObject.SetActive(false);
    }
}
