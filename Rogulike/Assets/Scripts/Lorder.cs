using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lorder : MonoBehaviour{

    public GameObject gameManager;
    public GameObject soundManager;

    public void Awake(){
        
        if(GameManager.instance == null){

            Instantiate(gameManager);
        }
        if (SoundManager.instance == null) {

            Instantiate(soundManager);
        }
    }
}
